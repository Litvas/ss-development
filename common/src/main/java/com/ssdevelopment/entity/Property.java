/**
 * Created by AS on 22.07.16.
 */

package com.ssdevelopment.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "properties")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "property_seq", allocationSize = 1, initialValue = 1)
public class Property {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @Column(name = "idpropety")
    private Long idProperty;

    @Column
    private String propertyTitle;

    @ManyToMany
    private Set<Category> categoryList;

    public Long getIdProperty() {
        return idProperty;
    }

    public void setIdProperty(Long idProperty) {
        this.idProperty = idProperty;
    }

    public String getPropertyTitle() {
        return propertyTitle;
    }

    public void setPropertyTitle(String propertyTitle) {
        this.propertyTitle = propertyTitle;
    }

    public Set<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(Set<Category> categoryList) {
        this.categoryList = categoryList;
    }
}
