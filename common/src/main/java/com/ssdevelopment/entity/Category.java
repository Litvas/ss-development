/**
 * Created by AS on 22.07.16.
 */

package com.ssdevelopment.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "categories")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "category_seq", allocationSize = 1, initialValue = 1)
public class Category {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @Column(name = "idcategory")
    private Long idCategory;

    @Column
    private String categoryTitle;

    public Category() {
    }

    public Long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Long idCategory) {
        this.idCategory = idCategory;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

//    public List<Category> getSubCategories() {
//        return subCategories;
//    }
//
//    public void setSubCategories(List<Category> subCategories) {
//        this.subCategories = subCategories;
//    }
}
