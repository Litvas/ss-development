package com.ssdevelopment.service;

import com.ssdevelopment.entity.Property;

/**
 * Created by AS on 28.07.16.
 */
public interface PropertyService extends BasicService<Property> {
}
