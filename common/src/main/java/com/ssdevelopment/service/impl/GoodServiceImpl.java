/**
 * Created by AS on 24.07.16.
 */

package com.ssdevelopment.service.impl;

import com.ssdevelopment.dao.GoodDAO;
import com.ssdevelopment.entity.Category;
import com.ssdevelopment.entity.Good;
import com.ssdevelopment.service.BasicService;
import com.ssdevelopment.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodServiceImpl implements GoodService {

    @Autowired
    private GoodDAO goodDAO;

    @Override
    public List<Good> getAll() {
        return goodDAO.getAll();
    }

    @Override
    public Good readOne(Long idGood) {
        return goodDAO.readOne(idGood);
    }

    @Override
    public Good addNew(Good good) {
        return goodDAO.addNew(good);
    }

    @Override
    public Good update(Good good) {
        return goodDAO.update(good);
    }

    @Override
    public void remove(Long id) {
        goodDAO.remove(id);
    }

}
