/**
 * Created by AS on 28.07.16.
 */

package com.ssdevelopment.service.impl;

import com.ssdevelopment.dao.CategoryDAO;
import com.ssdevelopment.entity.Category;
import com.ssdevelopment.entity.Good;
import com.ssdevelopment.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDAO categoryDAO;

    @Override
    public List<Category> getAll() {
        return categoryDAO.getAll();
    }

    @Override
    public Category readOne(Long id) {
        return categoryDAO.readOne(id);
    }

    @Override
    public Category addNew(Category entity) {
        return categoryDAO.addNew(entity);
    }

    @Override
    public Category update(Category entity) {
        return categoryDAO.update(entity);
    }

    @Override
    public void remove(Long id) {
        categoryDAO.remove(id);
    }

}
