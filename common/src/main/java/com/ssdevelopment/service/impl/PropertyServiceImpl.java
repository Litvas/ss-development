/**
 * Created by AS on 28.07.16.
 */

package com.ssdevelopment.service.impl;

import com.ssdevelopment.dao.PropertyDAO;
import com.ssdevelopment.entity.Property;
import com.ssdevelopment.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PropertyServiceImpl implements PropertyService {

    @Autowired
    private PropertyDAO propertyDAO;

    @Override
    public List<Property> getAll() {
        return propertyDAO.getAll();
    }

    @Override
    public Property readOne(Long id) {
        return propertyDAO.readOne(id);
    }

    @Override
    public Property addNew(Property entity) {
        return propertyDAO.addNew(entity);
    }

    @Override
    public Property update(Property entity) {
        return propertyDAO.update(entity);
    }

    @Override
    public void remove(Long id) {
        propertyDAO.remove(id);
    }
}
