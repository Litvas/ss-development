/**
 * Created by AS on 26.07.16.
 */

package com.ssdevelopment.service;

import com.ssdevelopment.entity.Good;

public interface GoodService extends BasicService<Good> {
}
