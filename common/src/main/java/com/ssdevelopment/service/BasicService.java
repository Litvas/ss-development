/**
 * Created by AS on 24.07.16.
 */

package com.ssdevelopment.service;

import com.ssdevelopment.entity.Good;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BasicService<T> {

    List<T> getAll();

    T readOne(Long id);

    T addNew(T t);

    T update(T t);

    void remove(Long id);


}
