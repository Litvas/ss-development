/**
 * Created by AS on 28.07.16.
 */

package com.ssdevelopment.service;

import com.ssdevelopment.entity.Category;

public interface CategoryService extends BasicService<Category> {
}
