/**
 * Created by AS on 28.07.16.
 */

package com.ssdevelopment.dao;

import com.ssdevelopment.entity.Property;


public interface PropertyDAO extends BasicDAO<Property> {
}
