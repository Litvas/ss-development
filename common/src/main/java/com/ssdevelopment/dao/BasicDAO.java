/**
 * Created by AS on 28.07.16.
 */

package com.ssdevelopment.dao;

import com.ssdevelopment.entity.Good;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BasicDAO<T> {

    List<T> getAll();

    T readOne(Long id);

    T addNew(T t);

    T update(T t);

    void remove(Long id);

}
