/**
 * Created by AS on 23.07.16.
 */

package com.ssdevelopment.dao.impl;

import com.ssdevelopment.dao.GoodDAO;
import com.ssdevelopment.dao.HibernateSessionFactory;
import com.ssdevelopment.entity.Good;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public class GoodDAOImpl implements GoodDAO {

    private HibernateSessionFactory hsf = new HibernateSessionFactory();
    private Session session = null;

    @Override
    public List<Good> getAll() {
        session = hsf.getSession();
        session.beginTransaction();
        List<Good> list =
                (List<Good>) session
                        .createCriteria(Good.class)
                        .addOrder(Order.asc("id"))
                        .list();
        session.getTransaction().commit();
        hsf.getSession().close();
        return list;
    }

    @Override
    public Good readOne(Long id) {
        session = hsf.getSession();
        session.beginTransaction();
        Good good = (Good) session.get(Good.class, id);
        session.getTransaction().commit();
        hsf.getSession().close();
        return good;
    }

    @Override
    public Good addNew(Good good) {
        session = hsf.getSession();
        session.beginTransaction();
        session.save(good);
        session.getTransaction().commit();
        hsf.getSession().close();
        return good;
    }

    @Override
    public Good update(Good good) {
        session = hsf.getSession();
        session.beginTransaction();
        session.update(good);
        session.getTransaction().commit();
        hsf.getSession().close();
        return good;
    }
// It will refactored
    @Override
    public void remove(Long id) {
        session = hsf.getSession();
        session.beginTransaction();
        Good good = (Good) session.load(Good.class, id);
        session.delete(good);
        session.getTransaction().commit();
        hsf.getSession().close();
    }
}
