/**
 * Created by AS on 28.07.16.
 */

package com.ssdevelopment.dao.impl;

import com.ssdevelopment.dao.HibernateSessionFactory;
import com.ssdevelopment.dao.PropertyDAO;
import com.ssdevelopment.entity.Good;
import com.ssdevelopment.entity.Property;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PropertyDAOImpl implements PropertyDAO {

    private HibernateSessionFactory hsf = new HibernateSessionFactory();
    private Session session = null;

    @Override
    public List<Property> getAll() {
        session = hsf.getSession();
        session.beginTransaction();
        List<Property> list =
                (List<Property>) session
                        .createCriteria(Property.class)
                        .addOrder(Order.asc("id"))
                        .list();
        session.getTransaction().commit();
        hsf.getSession().close();
        return list;
    }

    @Override
    public Property readOne(Long id) {
        session = hsf.getSession();
        session.beginTransaction();
       Property property = (Property) session.get(Property.class, id);
        session.getTransaction().commit();
        hsf.getSession().close();
        return property;
    }

    @Override
    public Property addNew(Property property) {
        session = hsf.getSession();
        session.beginTransaction();
        session.save(property);
        session.getTransaction().commit();
        hsf.getSession().close();
        return property;
    }

    @Override
    public Property update(Property property) {
        session = hsf.getSession();
        session.beginTransaction();
        session.update(property);
        session.getTransaction().commit();
        hsf.getSession().close();
        return property;
    }

    // It will refactored
    @Override
    public void remove(Long id) {
        session = hsf.getSession();
        session.beginTransaction();
        Property property = (Property) session.load(Property.class, id);
        session.delete(property);
        session.getTransaction().commit();
        hsf.getSession().close();
    }
}
