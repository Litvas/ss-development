/**
 * Created by AS on 28.07.16.
 */

package com.ssdevelopment.dao.impl;

import com.ssdevelopment.dao.CategoryDAO;
import com.ssdevelopment.dao.HibernateSessionFactory;
import com.ssdevelopment.entity.Category;
import com.ssdevelopment.entity.Good;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryDAOImpl implements CategoryDAO {

    private HibernateSessionFactory hsf = new HibernateSessionFactory();
    private Session session = null;

    @Override
    public List<Category> getAll() {
        session = hsf.getSession();
        session.beginTransaction();
        List<Category> list =
                (List<Category>) session
                        .createCriteria(Category.class)
                        .addOrder(Order.asc("id"))
                        .list();
        session.getTransaction().commit();
        hsf.getSession().close();
        return list;
    }

    @Override
    public Category readOne(Long id) {
        session = hsf.getSession();
        session.beginTransaction();
        Category category = (Category) session.get(Category.class, id);
        session.getTransaction().commit();
        hsf.getSession().close();
        return category;
    }

    @Override
    public Category addNew(Category category) {
        session = hsf.getSession();
        session.beginTransaction();
        session.save(category);
        session.getTransaction().commit();
        hsf.getSession().close();
        return category;
    }

    @Override
    public Category update(Category category) {
        session = hsf.getSession();
        session.beginTransaction();
        session.update(category);
        session.getTransaction().commit();
        hsf.getSession().close();
        return category;
    }

    // It will refactored
    @Override
    public void remove(Long id) {
        session = hsf.getSession();
        session.beginTransaction();
        Category category = (Category) session.load(Category.class, id);
        session.delete(category);
        session.getTransaction().commit();
        hsf.getSession().close();
    }
}
