package com.ssdevelopment.dao;

import com.ssdevelopment.entity.Good;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodDAO extends BasicDAO<Good> {

}
