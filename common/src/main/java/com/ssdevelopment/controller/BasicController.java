package com.ssdevelopment.controller;

import com.ssdevelopment.entity.Good;

import java.util.List;

/**
 * Created by AS on 27.07.16.
 */
public interface BasicController<T> {

    List<T> getAll();

    T getOne(Long id);

    T save(T t);

    T update(T t);

    void remove(Long id);
}
