/**
 * Created by AS on 28.07.16.
 */

package com.ssdevelopment.controller;

import com.ssdevelopment.entity.Category;
import com.ssdevelopment.entity.Good;
import com.ssdevelopment.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ssdevelopment")
public class CategoryController implements BasicController<Category> {

    @Autowired
    private CategoryService categoryService;

    @Override
    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public List<Category> getAll() {
        return categoryService.getAll();
    }

    @Override
    @RequestMapping(value = "/categories/{id}", method = RequestMethod.GET)
    public Category getOne(@PathVariable("id") Long id) {
        return categoryService.readOne(id);
    }

    @Override
    @RequestMapping(value = "/categories", method = RequestMethod.POST)
    @ResponseBody
    public Category save(@RequestBody @Valid Category category) {
        return categoryService.addNew(category);
    }

    @Override
    @RequestMapping(value = "/categories", method = RequestMethod.PUT)
    @ResponseBody
    public Category update(@RequestBody @Valid Category category) {
        return categoryService.update(category);
    }

    @Override
    @RequestMapping(value = "/categories/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void remove(@PathVariable("id") Long id) {
        categoryService.remove(id);
    }
}
