/**
 * Created by AS on 23.07.16.
 */

package com.ssdevelopment.controller;

import com.ssdevelopment.entity.Good;
import com.ssdevelopment.service.BasicService;
import com.ssdevelopment.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ssdevelopment")
public class GoodsController implements BasicController<Good> {

    @Autowired
    private GoodService goodService;

    @Override
    @RequestMapping(value = "/goods", method = RequestMethod.GET)
    public List<Good> getAll() {
        return goodService.getAll();
    }

    @Override
    @RequestMapping(value = "/goods/{id}", method = RequestMethod.GET)
    public Good getOne(@PathVariable ("id") Long id) {
        return goodService.readOne(id);
    }

    @Override
    @RequestMapping(value = "/goods", method = RequestMethod.POST)
    @ResponseBody
    public Good save(@RequestBody @Valid Good good) {
        return goodService.addNew(good);
    }

    @Override
    @RequestMapping(value = "/goods", method = RequestMethod.PUT)
    @ResponseBody
    public Good update(@RequestBody @Valid Good good) {
        return goodService.update(good);
    }

    @Override
    @RequestMapping(value = "/goods/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void remove(@PathVariable ("id") Long id) {
        goodService.remove(id);
    }
}
