/**
 * Created by AS on 28.07.16.
 */

package com.ssdevelopment.controller;

import com.ssdevelopment.entity.Good;
import com.ssdevelopment.entity.Property;
import com.ssdevelopment.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ssdevelopment")
public class PropertyController implements BasicController<Property> {

    @Autowired
    private PropertyService propertyService;

    @Override
    @RequestMapping(value = "/properties", method = RequestMethod.GET)
    public List<Property> getAll() {
        return propertyService.getAll();
    }

    @Override
    @RequestMapping(value = "/properties/{id}", method = RequestMethod.GET)
    public Property getOne(@PathVariable("id") Long id) {
        return propertyService.readOne(id);
    }

    @Override
    @RequestMapping(value = "/properties", method = RequestMethod.POST)
    @ResponseBody
    public Property save(@RequestBody @Valid Property property) {
        return propertyService.addNew(property);
    }

    @Override
    @RequestMapping(value = "/properties", method = RequestMethod.PUT)
    @ResponseBody
    public Property update(@RequestBody @Valid Property property) {
        return propertyService.update(property);
    }

    @Override
    @RequestMapping(value = "/properties/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void remove(@PathVariable ("id") Long id) {
        propertyService.remove(id);
    }
}
