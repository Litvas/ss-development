/**
 * Created by AS on 06.08.16.
 */


var xmlhttp = new XMLHttpRequest();
var url = "http://127.0.0.1:8080/ssdevelopment/goods";
xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        getAllEmployees(xmlhttp.responseText);
    }
};

xmlhttp.withCredentials = true;
xmlhttp.open("GET", url, true);
xmlhttp.send();

function getAllEmployees(response) {
    var arr = JSON.parse(response);
    var out = "<table>";
    for (var i = 0; i < arr.length; i++) {
        out += "<tr>" +
            "<td>" + arr[i].idGood + "</td>" +
            "<td>" + arr[i].title + "</td>" +
            "<td>" + arr[i].price + "</td>" +
            "<td>" + arr[i].description + "</td>" +
            "<td>" + arr[i].quantity + "</td>" +
            "<td>" + arr[i].category.categoryTitle + "</td>" +
            "<td><button type='button' onclick=return(getOneGood('http://127.0.0.1:8080/ssdevelopment/goods/" + arr[i].idGood + "'))>Edit employee</button></td></tr>";
    }
    out += "</table>";
    document.getElementById("tagForPaste").innerHTML = out;
}


function getOneGood(text) {
    url = text;
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            parseOneGood(xmlhttp.responseText);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function parseOneGood(response) {
    var arrOne = JSON.parse(response);
    document.getElementById("tagForPaste").innerHTML = "";
    document.getElementById("tagForPaste").innerHTML = "<table>" +
        "<tr><td><input name='title' id='title' value='" + arrOne.title + "'></td></tr>" +
        "<tr><td><input name='price' id='price' value='" + arrOne.price + "'></td></tr>" +
        "<tr><td><input name='description' id='description' value='" + arrOne.description + "'></td></tr>" +
        "<tr><td><input name='quantity' id='quantity'  value='" + arrOne.quantity + "'></td></tr>" +
        "<tr><td><button type='button' onclick='saveData()'>"
    "</table>";
}


function saveData() {
    var arrOne = {title: document.getElementById("title").value,
        price: document.getElementById("price").value};

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            parseOneGood(xmlhttp.responseText);
        }
    };
    xmlhttp.open("POST", "http://127.0.0.1:8080/ssdevelopment/goods", true);
    xmlhttp.send();

    alert("sent!");
}
